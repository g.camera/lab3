package it.cipi.labs.service.logic;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;

import it.cipi.labs.service.data.RainData;
import it.cipi.labs.service.utilities.SSSUtilities;

public class VirtualObjectCore {

	public void processData (RainData rd, ServletContext sc) {
		if(rd.getWaterLevel()>10) {
			//store dati critici
			ConcurrentHashMap data= (ConcurrentHashMap) sc.getAttribute("DataStoreObject");
			Long timestamp= System.currentTimeMillis();
			data.put(rd.getWeatherStationID()+"_"+timestamp.toString(), rd.getWaterLevel());
			Logger.getRootLogger().info(rd.toString() + " added to datastoreobject");
			Iterator it = data.entrySet().iterator();
		    while (it.hasNext()) {
		        Map.Entry pair = (Map.Entry)it.next();
		        System.out.println(pair.getKey() + " = " + pair.getValue());
		    }
		    
		    SSSUtilities sssUtilities= (SSSUtilities) sc.getAttribute("SSSUtilities");
		    String [][] table = new String [1][1];
		    table[0][0]=rd.toString();
			Set<String> recipients = new HashSet<String>();
			recipients.add("g.camera@cipi.unige.it");
		    sssUtilities.createPrivateView(rd.getWeatherStationID()+"_"+timestamp.toString(), table, recipients);
			
			
		}
	}
}
