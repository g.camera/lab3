package it.cipi.labs.service.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RainData {
	
	public RainData(String weatherStationID, String location, int waterLevel) {
		super();
		this.weatherStationID = weatherStationID;
		this.location = location;
		this.waterLevel = waterLevel;
	}
	
	

	@Override
	public String toString() {
		return "RainData [weatherStationID=" + weatherStationID + ", location=" + location + ", waterLevel="
				+ waterLevel + "]";
	}



	@SerializedName("weatherStationID")
	@Expose
	private String weatherStationID;
	
	@SerializedName("location")
	@Expose
	private String location;
	
	@SerializedName("waterLevel")
	@Expose
	private int waterLevel;

	public String getWeatherStationID() {
		return weatherStationID;
	}

	public void setWeatherStationID(String weatherStationID) {
		this.weatherStationID = weatherStationID;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public int getWaterLevel() {
		return waterLevel;
	}

	public void setWaterLevel(int waterLevel) {
		this.waterLevel = waterLevel;
	}
	
	
	
	
	
}
