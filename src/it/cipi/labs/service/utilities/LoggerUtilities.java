package it.cipi.labs.service.utilities;

import java.util.Enumeration;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.RollingFileAppender;

/**
 * Logger Utilities for log4j logger management
 * 
 * @author Giancarlo_Camera
 *
 */
public class LoggerUtilities {

	public static Logger initLogger() {
		Logger logger = initLogger(Level.ALL, LoggerAppender.ConsoleAppender, null);
		return logger;
	}

	public static Logger initLogger(Level loggerLevel, LoggerAppender appenderType, String filePath) {
		Logger logger = Logger.getRootLogger();
		if (!logger.getAllAppenders().hasMoreElements()) {
			try {
				switch (appenderType) {
				case ConsoleAppender:
					addConsoleAppender();
					break;
				case RollingFileAppender:
					addRollingFileAppender(filePath);
					break;
				}

				logger.info("Logger initialized");

			} catch (Exception e) {
				e.printStackTrace(System.out);

			}
		} else {
			logger.warn("Logger already initialized, call add appender functions to add appenders");
		}
		return logger;
	}

	public static void addRollingFileAppender(String filePath) {

		if (!hasAppender(LoggerAppender.RollingFileAppender)) {
			if (filePath != null) {
				RollingFileAppender rfa = new RollingFileAppender();
				rfa.setName(filePath.substring(filePath.lastIndexOf('/') + 1));
				rfa.setImmediateFlush(true);
				rfa.setFile(filePath);
				rfa.setLayout(new PatternLayout("%d{dd MMM yyyy HH:mm:ss} %5p %l %m%n"));
				rfa.setThreshold(Level.DEBUG);
				rfa.setAppend(true);
				rfa.setMaxFileSize("10MB");
				rfa.setMaxBackupIndex(50);
				rfa.activateOptions();
				Logger.getRootLogger().addAppender(rfa);

			} else {
				System.out.println("No file path specified");
			}
		} else {
			Logger.getRootLogger().warn("Console Logger already initialized");
		}

	}

	public static void addConsoleAppender() {
		if (!hasAppender(LoggerAppender.ConsoleAppender)) {
			ConsoleAppender ca = new ConsoleAppender();
			String PATTERN = "%d{dd MMM yyyy HH:mm:ss} %5p %l %m%n";
			ca.setLayout(new PatternLayout(PATTERN));
			ca.setThreshold(Level.ALL);
			ca.activateOptions();
			Logger.getRootLogger().addAppender(ca);
		} else {
			Logger.getRootLogger().warn("Console Logger already initialized");
		}

	}

	@SuppressWarnings("rawtypes")
	public static boolean hasAppender(LoggerAppender appenderType) {
		for (Enumeration e = Logger.getRootLogger().getAllAppenders(); e.hasMoreElements();)
			if (e.nextElement().getClass().getName().endsWith(appenderType.toString()))
				return true;
		return false;
	}

}
