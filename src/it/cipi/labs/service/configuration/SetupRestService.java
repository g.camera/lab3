package it.cipi.labs.service.configuration;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("rest")
public class SetupRestService extends ResourceConfig{
	
	public SetupRestService () {
		packages("it.cipi.labs.service.rest");
		
	}
	

}
